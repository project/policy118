Policy118 is a simple and lightweight registration and profile form password
policy. What this module does for you is to add three password validation checks
for visitors registering on your website and when users want to update their
password on the user profile page. When they get on the register form page,
instead of only seeing a username and email input, the module adds two password
fields (on the user profile page it simply adds the javascript validation to the
password fields) where the first field accepts a password and checks if the
policy requirements are met and the second password field checks if the
passwords match.

The policy check is written in jquery and asynchronously displays if the policy
requirements are met and if the passwords match. By default the module disables
the form submition on the user register form until the policy requirements are
met and the two passwords match. On the user profile form instead the module
disables form submition only if one of the two password fields aren't empty.

Policy118's name derives from the policy checks it applies on the first password
field which are:

-there has to be at least 1 uppercase letter
-there has to be at least 1 number
-the password needs to be at least 8 characters long

The module is fairly simple to use, just install it and enable it, no extra
configuration needed. This module was written for those websites that need a
simple and lightweight password validation on their registration and user
profile forms. If you are looking for more complex solutions please consider
other modules instead.
