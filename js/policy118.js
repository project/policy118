/**
 * @file
 * Enforces policy118 validation.
 */

(function ($) {

  'use strict';

  /**
   * Attach handlers to evaluate the policy118 conditions.
   *
   * Evaluates the user registration and user profile password fields and checks
   * if user's input complies with policy118 conditions and the passwords match.
   */
  Drupal.behaviors.password = {
    attach: function (context, settings) {
      var translate = settings.password;
      $('input.password-field', context).once('password', function () {
        var passwordInput = $(this);
        var outerWrapper = $(this).parent().parent();
        // Sets the submit button to disabled and greyed out.
        var submitDisabled = function () {
          $('#edit-submit').attr('disabled', 'disabled').addClass('disabled');
          $('input.disabled').css('background-color', '#D0D0D0');
        };
        // Sets the submit button to submittable.
        var submitEnabled = function () {
          $('#edit-submit').removeAttr('style').removeAttr('disabled');
        };
        // Gets the form Id to perform a check later on if it's the
        // user-register-form or user-profile-form.
        var formId = $(this).closest('form').attr('id');
        // Sets a policy check variable and a password confirm variable to
        // evaluate true or false if the policy is met
        // (used as a checkbox throughout the function).
        var policy118Check = '';
        var policy118ConfirmPass = '';

        var confirmMatch = '';
        // Disables the submit form by default on the user-register-form.
        if (formId === 'user-register-form') {
          submitDisabled();
        }

        // Add the password confirmation layer.
        $('.form-item-pass-pass2').filter('div:last').append('<div class="password-confirm-result">' + translate.confirmTitle + ' <span></span></div>').addClass('confirm-parent');
        var confirmInput = $('input.password-confirm', outerWrapper);
        var confirmResult = $('div.password-confirm-result', outerWrapper);
        var confirmChild = $('span', confirmResult);

        // Sets the password-confirm-result div visibility to hidden by default.
        confirmResult.css({visibility: 'hidden'});

        // Add the description box.
        $(confirmInput).parent().after('<div class="password-suggestions description"></div>');
        var passwordDescription = $('div.password-suggestions', outerWrapper).hide();

        var passwordCheck = function () {

          if (passwordInput.val() === '' && formId === 'user-profile-form' && policy118ConfirmPass === '') {
            submitEnabled();
            passwordDescription.hide();
          }

          else {
            // Evaluate the password policy.
            var result = Drupal.evaluatePasswordPolicy(passwordInput.val(), settings.password);

            // Update the suggestions for how to improve the password.
            if (passwordDescription.html() !== result.message) {
              passwordDescription.html(result.message);
            }
            passwordDescription.show();

            // Checks if all policy118 conditions check out and if password
            // matches and enables or disables the submit button accordingly.
            if (result.passwordToTest.length >= 8 && result.hasUppercase === true && result.hasNumbers === true && confirmMatch === 'ok') {
              submitEnabled();
              policy118Check = 1;
            }
            else {
              submitDisabled();
              policy118Check = '';
            }
          }
        };

        // Check that password and confirmation inputs match.
        var passwordCheckMatch = function () {
          // Checks if it's the user-profile-form and enables back the submit
          // button if the input is empty.
          if (confirmInput.val() === '' && formId === 'user-profile-form') {
            policy118ConfirmPass = '';
            confirmResult.css({visibility: 'hidden'});

            if (passwordInput.val() === '') {
              submitEnabled();
            }
            else {
              submitDisabled();
            }
          }

          else {
            policy118ConfirmPass = 1;
            if (policy118Check === '' || (confirmInput.val() === '' && (formId === 'user-register-form' || policy118Check === 1))) {
              submitDisabled();
            }

            if (confirmInput.val()) {
              var success = passwordInput.val() === confirmInput.val();

              // Show the confirm result.
              confirmResult.css({visibility: 'visible'});

              // Remove the previous styling if any exists.
              if (this.confirmClass) {
                confirmChild.removeClass(this.confirmClass);
              }

              // Fill in the success message and set the class accordingly.
              var confirmClass = success ? 'ok' : 'error';
              confirmChild.html(translate['confirm' + (success ? 'Success' : 'Failure')]).addClass(confirmClass);
              this.confirmClass = confirmClass;
              // Set the confirmMatch variable to whatever value confirmClass has
              // to be used in the policy conditions check in the function above.
              confirmMatch = confirmClass;
            }
            else {
              confirmResult.css({visibility: 'hidden'});
            }
          }
        };

        // Monitor keyup, keydown and blur events.
        // Blur must be used because a mouse paste does not trigger keyup.
        // Keyup is used for delete and backspace usage for clearing the input.
        passwordInput.keyup(passwordCheck).keydown(passwordCheck).focus(passwordCheck).blur(passwordCheck).keyup(passwordCheckMatch);
        confirmInput.keyup(passwordCheckMatch).blur(passwordCheckMatch).keyup(passwordCheck).keydown(passwordCheckMatch);
      });
    }
  };

  /**
   * Evaluate the password policy118 and returns the relevant output message.
   *
   * @param password
   * @param translate
   *
   * @returns {{message: Array, passwordToTest: *, hasUppercase: boolean, hasNumbers: boolean}}
   */
  Drupal.evaluatePasswordPolicy = function (password, translate) {
    password = $.trim(password);

    // Set the variables.
    var msg = [];
    var hasUppercase = /[A-Z]+/.test(password);
    var hasNumbers = /[0-9]+/.test(password);

    if (!hasUppercase) {
      msg.push('<div class="policy-not-ready">' + translate.addUpperCase + '</div><br/>');
    }
    else {
      msg.push('<div><div class="addUpperCase"></div>' + translate.addUpperCase.fontcolor('green') + '</div><br/>');
    }
    if (!hasNumbers) {
      msg.push('<div class="policy-not-ready">' + translate.addNumbers + '</div><br/>');
    }
    else {
      msg.push('<div><div class="addNumbers"></div>' + translate.addNumbers.fontcolor('green') + '</div><br/>');
    }
    if (password.length < 8) {
      msg.push('<div class="policy-not-ready">' + translate.tooShort + '</div><br/>');
    }
    else {
      msg.push('<div><div class="tooShort"></div>' + translate.tooShort.fontcolor('green') + '</div><br/>');
    }

    // Assemble the final message.
    msg = '<h3>' + translate.hasWeaknesses + '</h3><br/>' + msg.join('');
    return {message: msg, passwordToTest: password, hasUppercase: hasUppercase, hasNumbers: hasNumbers};
  };

})(jQuery);
